import sqlite3


class DBConnection:
    """ DB connection class """

    def connect(self):
        """ Method creates DB and table if they are not exist
        and returns:
        conn - connection to the DB
        cursor - object for work with DB
        """
        conn = sqlite3.connect("DB.db")
        cursor = conn.cursor()
        try:
            while True:
                cursor.execute("""CREATE TABLE pictures
                (url text, size integer, download_time integer)
                """)
                break
        finally:
            return conn, cursor
