from os import path
from src.db_connection import DBConnection
from urllib.request import Request, urlopen


class DownloadPicture:
    """ Class contains methods to downloads a picture
    and makes record in the DataBase about new picture data
    """
    url = "https://randomfox.ca/floof/"

    def db_record(self):
        """ Method makes new data record in the DB """
        url, size, time = self.download()
        conn, cursor = DBConnection().connect()
        cursor.execute("""INSERT INTO pictures
                  VALUES ('%s', '%s', '%s')""" % (url, size, time))
        conn.commit()
        conn.close()

    def download(self):
        """ Method downloads image and returns:
        image url, size and download time
        """
        image_url = self.get_img_url()
        image_name = image_url[27:]
        req = Request(image_url, headers={'User-Agent': 'Mozilla/5.0'})
        img = urlopen(req)
        out = open(image_name, 'wb')
        out.write(img.read())
        out.close()
        size = path.getsize(image_name)
        time = path.getctime(image_name)
        return image_url, size, time

    def get_img_url(self):
        """ Method returns image url """
        req = Request(self.url, headers={'User-Agent': 'Mozilla/5.0'})
        web_byte = urlopen(req).read()
        data = web_byte.decode('utf-8')
        s = data[10:]
        s = s[:s.find('","link"')]
        img_url = s.replace("\/", "/")
        return img_url


if __name__ == "__main__":
    """ Trigger code """
    runner = DownloadPicture()
    runner.db_record()
