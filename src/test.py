import os
import glob
from src.download_picture import DownloadPicture
from src.db_connection import DBConnection


class Test:
    """ Class contains 5 test methods """

    def test_url_connection(self):
        """ Method checks download image url """
        url = DownloadPicture().get_img_url()
        actual_value = url[:27]
        expected_value = "http://randomfox.ca/images/"
        if actual_value == expected_value:
            print("1 PASS: URL is correct.")

    def test_db_connection(self):
        """ Method tries to became access to the DB"""
        conn, cursor = DBConnection().connect()
        try:
            cursor.execute("SELECT * FROM pictures")
            while True:
                print("2 PASS: Connection with DB established.")
                break
        finally:
            conn.close

    def test_with_no_db(self):
        """ Method tries execute code with no DB """
        file = "DB.db"
        if os.path.isfile(file):
            os.remove(file)
            DownloadPicture().download()
            print("3 PASS: The code with no DB works correctly.")

    def test_with_db(self):
        """ Method tries execute code with DB """
        if os.path.isfile("DB.db"):
            DownloadPicture().download()
        else:
            DBConnection().connect()
            DownloadPicture().download()
        print("4 PASS: The code with DB works correctly.")

    def test_file_access(self):
        """ Method tries to become access to the images """
        new_name = "my_img.jpg"
        for img in glob.glob("*.jpg"):
            os.rename(img, new_name)
            break
        if os.path.isfile(new_name):
            os.remove(new_name)
            print("5 PASS: Access to the file is success.")


if __name__ == "__main__":
    """ Trigger code """
    runner = Test()
    runner.test_url_connection()
    runner.test_db_connection()
    runner.test_with_no_db()
    runner.test_with_db()
    runner.test_file_access()
